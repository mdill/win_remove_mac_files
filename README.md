# Windows Remove Mac Files

## Purpose

Apple's OSx is notorious for leaving "hidden" dot-files throughout each
directory.  These are usually thumbnail information, and GUI settings for OSx's
directories.

As a Windows/Linux user, these can clutter a server's directories, and make for
an annoying interface.  Instead of working through an entire tree of
child-directories, place this PowerShell script in the parent directory, open
it, and let it run.

It will work its way down ever child directory, searching for and deleting these
OSx files.

## Download

    cd ~/
    git clone https://bitbucket.org/mdill/win_remove_mac_files.git

## License

This project is licensed under the BSD License - see the [LICENSE.md](https://bitbucket.org/mdill/win_remove_mac_files/src/e714dde1f3244aa559d1128d5d6a71617a1d4f6a/LICENSE.txt?at=master) file for
details.

